import { NgModule } from '@angular/core';

import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon'
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
	imports: [
		MatDialogModule,
		MatInputModule,
		MatButtonModule,
		MatSelectModule,
		MatTableModule
	],
	exports: [
		MatDialogModule,
		MatInputModule,
		MatButtonModule,
		MatSelectModule,
		MatTableModule,
		MatCardModule,
		MatIconModule,
		MatDividerModule,
		MatProgressSpinnerModule
	],
	providers: [{
		provide: MatDialogRef,
		useValue: {}
	}, {
		provide: MAT_DIALOG_DATA,
		useValue: {}
	}]
})
export class MaterialModule { }
