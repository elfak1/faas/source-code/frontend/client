import { HostListener, Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../../utility/services/base.service';
import { ServiceUrls } from 'src/environments/environment';
import { interval, Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/platform/models/user.model';

@Injectable({
	providedIn: 'root'
})
export class AuthService extends BaseService implements OnDestroy {

	private subscription: Subscription;
	user: User;
	private timer: Observable<any>;

	constructor(http: HttpClient) {
		super(http);
	}

	ngOnDestroy(): void {
		if (this.subscription != null) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}
	}

	signin(value: User): Observable<boolean> {
		this.user = value;
		this.keepLoggedIn();
		return this.post(ServiceUrls.Signin, value).pipe(map(x => {
			console.log("Sign in successful");
			localStorage.setItem('token', x.data.jwt);
			return true;
		}));
	}

	signup(value: User) {
		this.user = value;
		this.keepLoggedIn();
		return this.post(ServiceUrls.SignUp, value).pipe(map(x => {
			console.log("Sign up successful");
			localStorage.setItem('token', x.data.jwt);
			return true;
		}));
	}

	logOut() {
		localStorage.removeItem('token');
		if (this.subscription != null) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}
		this.timer = null;
		this.user = null;
	}

	keepLoggedIn() {
		if (this.subscription == null && this.user != null) {
			this.timer = interval(600 * 1000);
			console.log("Token subscription created");
			this.subscription = this.timer.subscribe(() => {
				console.log("Token refreshed");
				this.signin(this.user);
			});
		}
	}

	@HostListener('window:beforeunload', ['$event'])
	beforeunloadHandler(event) {
		this.logOut();
	}
}
