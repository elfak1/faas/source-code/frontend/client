import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { BaseService } from './base.service';
import { ServiceUrls } from 'src/environments/environment';
import { CreateProjectRequest } from 'src/app/platform/models/create-project-request.model';

import { webSocket } from "rxjs/webSocket";

@Injectable({
	providedIn: 'root'
})
export class ProjectService extends BaseService implements OnDestroy {

	constructor(http: HttpClient) {
		super(http);
		this.initializeSocketConnection();
	}

	ngOnDestroy(): void {
		if (this.subscription != null) {
			this.subscription.unsubscribe;
			this.subscription = null;
		}
	}

	createProject(value: CreateProjectRequest): Observable<any> {
		return this.post(ServiceUrls.CreateCode, value);
	}

	getProjects(): Observable<any> {
		return this.get(ServiceUrls.Project);
	}

	stopProject(projectId: number) {
		return this.put(`${ServiceUrls.Project}/${projectId.toString()}/stop`, null);
	}

	startProject(projectId: number) {
		return this.put(`${ServiceUrls.Project}/${projectId.toString()}/run`, null);
	}

	removeProject(projectId: number) {
		return this.delete(`${ServiceUrls.Project}/${projectId.toString()}`);
	}

	subject = webSocket('ws://elfakfaas.xyz:1912/faas-ws');
	observableTopic;
	subscription;

	initializeSocketConnection() {
		this.observableTopic = this.subject.multiplex(
			() => ({subscribe: '/topic'}), // When server gets this message, it will start sending messages for 'A'...
			() => ({unsubscribe: '/topic'}), // ...and when gets this one, it will stop.
			(message: any) => message.type === '/topic' // If the function returns `true` message is passed down the stream. Skipped if the function returns false.
		);

		this.subscription = this.observableTopic.subscribe(
			msg => console.log('message received: ' + msg), // Called whenever there is a message from the server.
			err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
			() => console.log('complete') // Called when connection is closed (for whatever reason).
		);
	}

}
