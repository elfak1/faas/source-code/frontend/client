import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class BaseService {

	options: { headers: HttpHeaders } = {
		headers: new HttpHeaders({
			'Access-Control-Allow-Origin': '*'
		})
	};

	constructor(private http: HttpClient) { }

	get(url: string): Observable<any> {
		this.generateOptions();
		return this.http.get<any>(url, this.options);
	}

	put(url: string, body: any): Observable<any> {
		this.generateOptions();
		return this.http.put<any>(url, body, this.options);
	}

	post(url: string, body: any): Observable<any> {
		this.generateOptions();
		return this.http.post<any>(url, body, this.options);
	}

	delete(url: string): Observable<void> {
		this.generateOptions();
		return this.http.delete<any>(url, this.options);
	}

	private generateOptions(): void {
		const token = localStorage.getItem('token');
		if (token) {
			this.options.headers = this.options.headers.delete('Authorization');
			this.options.headers = this.options.headers.append('Authorization', `Bearer ${token}`);
		}
	}
}

