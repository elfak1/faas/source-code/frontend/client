import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './utility/services/auth.service';
import { User } from './platform/models/user.model';

@Injectable()
export class MyInterceptor implements HttpInterceptor {

	constructor(private authService: AuthService) {}

	intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// const timestamp = localStorage.getItem('timestamp');
		// if (timestamp) {
		// 	const elapsedTime = (Date.now() - Number(timestamp)) / 1000 / 60;
		// 	if (elapsedTime > 10) {
		// 		const user: User = {
		// 			username: '',
		// 			email: localStorage.getItem('email'),
		// 			plainTextPassword: localStorage.getItem('plainTextPassword'),
		// 			token: ''
		// 		}
		// 		this.authService.signin(user).subscribe(x => {
		// 			if (x)
		// 				return next.handle(httpRequest);
		// 		});
		// 	}
		// 	else
		// 		return next.handle(httpRequest);
		// }
		// else
			return next.handle(httpRequest);
	}
}