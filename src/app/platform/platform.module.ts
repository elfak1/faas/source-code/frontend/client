import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { EditorModule } from '../editor/editor.module';

import { PlatformComponent } from './platform.component';

import { CoreComponent } from './components/core/core.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { HeaderComponent } from './components/header/header.component';
import { EditorComponent } from './components/editor/editor.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { FaasFunctionListComponent } from './components/faas-function-list/faas-function-list.component';
import { FaasFunctionListItemComponent } from './components/faas-function-list-item/faas-function-list-item.component';

import { MaterialModule } from '../material.module';
import { FunctionComponent } from './components/function/function.component';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { FunctionsOverviewComponent } from './components/functions-overview/functions-overview.component';
import { DependenciesComponent } from '../platform/components/dependencies/dependencies.component';
import { UrlPopupComponent } from './components/url-popup/url-popup.component';
import { AdditionalInfoComponent } from './components/additional-info/additional-info.component';

@NgModule({
	declarations: [
		CoreComponent,
		LogInComponent,
		HeaderComponent,
		EditorComponent,
		SignUpComponent,
		SidebarComponent,
		PlatformComponent,
		FunctionComponent,
		LandingPageComponent,
		AccountSettingsComponent,
		FaasFunctionListComponent,
		FunctionsOverviewComponent,
		FaasFunctionListItemComponent,
		DependenciesComponent,
		UrlPopupComponent,
		AdditionalInfoComponent
	],
	imports: [
		CommonModule,
		RouterModule,
		EditorModule,
		MaterialModule,
		ReactiveFormsModule
	],
	exports: [
		PlatformComponent,
		LandingPageComponent
	]
})
export class PlatformModule { }
