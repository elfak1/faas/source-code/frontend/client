import { FormControl } from '@angular/forms';
import { Component } from '@angular/core';

import { ProjectService } from 'src/app/utility/services/project.service';
import { ExtendedProject } from '../../models/extended-project.model';
import { finalize } from 'rxjs/operators';

@Component({
	selector: 'faas-functions-overview',
	templateUrl: './functions-overview.component.html',
	styleUrls: ['./functions-overview.component.scss']
})
export class FunctionsOverviewComponent {

	allProjectModels: Array<ExtendedProject>;
	displayedProjectModels: Array<ExtendedProject>;
	selectedProjectModel: ExtendedProject;
	projectFilterFormControl: FormControl = new FormControl();
	isBusy: boolean = false;
	selectedId : number = 0;

	onChangeProjectState(event: number) {
		const selectedProject = this.displayedProjectModels.find(x => x.id == event);
		selectedProject.deploymentStatus == "RUNNING" ? this.stopProject(selectedProject.id) : this.startProject(selectedProject.id);
	}

	stopProject(id: number) {
		this.projectService.stopProject(id).subscribe(x => {
			this.allProjectModels.find(x => x.id == id).deploymentStatus = "STOPPED";
		});
	}

	startProject(id: number) {
		this.projectService.startProject(id).subscribe(x => {
			this.allProjectModels.find(x => x.id == id).deploymentStatus = "STARTING";
		});
	}

	onRemoveProject(event: number) {
		// const selectedProjectIndex = this.displayedProjectModels.findIndex(x => x.id == event);
		// this.displayedProjectModels.splice(selectedProjectIndex, 1);
		this.projectService.removeProject(event).subscribe(x => {
				this.onRefresh();
			});
	}

	onProjectChosen(event: number) {
		this.selectedProjectModel = { ...this.displayedProjectModels.find(x => x.id == event) };
		this.selectedId = this.selectedProjectModel.id;
	}

	onRefresh() {
		this.isBusy = true;
		this.projectService.getProjects().pipe(finalize(() => this.isBusy = false)).subscribe(x => {
			console.log(x);
			this.allProjectModels = x.data.content.map(x => { return {
				id: x.id,
				name: x.name,
				description: x.description,
				projectOwnerProfileId: x.projectOwnerProfileId,
				programmingLanguage: x.programmingLanguage,
				deploymentStatus: x.deploymentStatus,
				code: x.userCode.code,
				dependencies: x.userCode.dependencies,
				executeEndpoint: x.executeEndpoint,
				codeInfoEndpoint: x.codeInfoEndpoint
			}});
			this.displayedProjectModels = this.allProjectModels;
			if (this.displayedProjectModels.length > 0)
				this.onProjectChosen(this.displayedProjectModels[0].id)
			else
				this.selectedProjectModel = new ExtendedProject();
		}, );
	}

	constructor(private projectService: ProjectService) {
		this.onRefresh();
		this.projectFilterFormControl.valueChanges.subscribe((x: string) =>
			this.displayedProjectModels = this.allProjectModels.filter(y =>
				y.name.toLowerCase().includes(x.toLowerCase())));
	}

}