import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'faas-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

	expanded: boolean = true;

	constructor() { }

	ngOnInit(): void {
	}

}
