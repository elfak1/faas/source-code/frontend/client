import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

import { User } from "../../models/user.model";
import { AuthService } from "src/app/utility/services/auth.service";

@Component({
	selector: 'faas-log-in',
	templateUrl: 'log-in.component.html',
	styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

	userForm: FormGroup;

	constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {}

	ngOnInit(): void {
		this.userForm = this.createForm();
	}

	createForm(): FormGroup {
		return this.fb.group({
			["email"]: new FormControl('', [Validators.email]),
			["password"]: new FormControl()
		});
	}

	public onLoginClicked(): void {
		const user = new User();
		user.email = this.userForm.get("email").value;
		user.plainTextPassword = this.userForm.get("password").value;
		console.log(user);
		this.auth.signin(user).subscribe(x => {
			if (x)
				this.router.navigate(["../../platform/editor"]);
		},error => alert("Login attempt failed"));
	}

}