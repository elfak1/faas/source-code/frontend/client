import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Dependency } from 'src/app/platform/models/dependency.model';
import { ProgrammingLanguage } from 'src/app/platform/models/programming-language.enum';
import { DependenciesComponent } from '../dependencies/dependencies.component';
import { ExtendedProject } from '../../models/extended-project.model';
import { CreateProjectRequest } from '../../models/create-project-request.model';
import { AdditionalInfoComponent } from '../additional-info/additional-info.component';

@Component({
	selector: 'faas-function',
	templateUrl: './function.component.html',
	styleUrls: ['./function.component.scss']
})
export class FunctionComponent implements OnInit {

	private _projectModel: ExtendedProject;
	@Input()
	set projectModel(value: ExtendedProject) {
		if (value != null) {
			this._projectModel = value;
			this.dependencyModels = this.projectModel.dependencies;
			this.projectFormGroup = new FormGroup({
				name: new FormControl(this._projectModel.name),
				code: new FormControl(this._projectModel.code),
				language: new FormControl(this.projectModel.programmingLanguage),
			});
			this.projectFormGroup.disable();
		}
	}
	get projectModel(): ExtendedProject {
		return this._projectModel;
	}

	@Output()
	projectSubmited: EventEmitter<any> = new EventEmitter();

	@Output()
	refreshEvent: EventEmitter<any> = new EventEmitter();

	@Input()
	readOnly: boolean;

	projectFormGroup: FormGroup = this.createProjectFormGroup();

	private _dependencyModels: Array<Dependency>;
	get dependencyModels(): Array<Dependency> {
		return this._dependencyModels;
	}
	set dependencyModels(value: Array<Dependency>) {
		this._dependencyModels = value;
	}

	createProjectFormGroup() {
		return new FormGroup({
			name: new FormControl("", Validators.required),
			code: new FormControl(""),
			language: new FormControl("", Validators.required),
		});
	}

	constructor(public dialog: MatDialog) { }

	supportedLanguages;

	ngOnInit(): void {
		this.supportedLanguages = Object.keys(ProgrammingLanguage)
			.filter(value => isNaN(Number(value)) === false)
			.map(key => ProgrammingLanguage[key]);
	}

	openDependenciesWindow() {
		if (this.readOnly && this.dependencyModels == null) {
			alert("Project has no dependencies");
			return;
		};

		this.dialog.open(DependenciesComponent, {
			disableClose: true,
			data: {
				dependencies: this.dependencyModels,
				readonly: this.readOnly
			}
		}).afterClosed().subscribe(x => {
			if (x != null && !this.readOnly)
				this.dependencyModels = x.data;
		});
	}

	onAditionalInfo() {
		this.dialog.open(AdditionalInfoComponent, {
			disableClose: true,
			data: {
				name: this.projectModel?.name,
				executeEndpoint: this.projectModel?.executeEndpoint,
				codeInfoEndpoint: this.projectModel?.codeInfoEndpoint
			}
		});
	}

	get dependencyButtonDisplayText() {
		return this.readOnly ? "View Dependencies" : "Configure Dependencies";
	}

	get isSubmitDisabled(): boolean {
		if (this.projectFormGroup.get('name').errors != null || this.projectFormGroup.get('language').errors != null)
			return true;
		return false;
	}

	onSubmit() {
		if (this.isSubmitDisabled)
			return;
		let createProjectRequest: CreateProjectRequest = {
			language: ProgrammingLanguage.Java,
			projectName: this.projectFormGroup.get("name").value,
			userCode: this.projectFormGroup.get("code").value,
			dependencies: this.dependencyModels
		}
		this.projectSubmited.emit({ request: createProjectRequest });
	}

	onRefresh() {
		this.refreshEvent.emit();
	}
}
