import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'faas-landing-page',
	templateUrl: './landing-page.component.html',
	styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent {

	signupVisible: boolean;
	loginVisible: boolean = true;

	constructor(router: Router) {
		router.events.subscribe(event => {
			if (event instanceof NavigationEnd)
				if (event.url.includes("login"))
					this.onLoginPage();
				else if (event.url.includes("signup"))
					this.onSignUpPage();
			});
	}
	onSignUpPage() {
		this.signupVisible = true;
		this.loginVisible = false;
	}
	onLoginPage() {
		this.signupVisible = false;
		this.loginVisible = true;
	}
}
