import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'faas-additional-info',
	templateUrl: './additional-info.component.html',
	styleUrls: ['./additional-info.component.scss']
})
export class AdditionalInfoComponent {

	constructor(public dialogRef: MatDialogRef<AdditionalInfoComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

	ngOnInit(): void {

	}

	close(): void {
		this.dialogRef.close();
	}

}
