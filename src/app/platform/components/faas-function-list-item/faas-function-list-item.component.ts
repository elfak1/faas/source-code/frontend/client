import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Project } from '../../models/project.model';

@Component({
	selector: 'faas-function-list-item',
	templateUrl: './faas-function-list-item.component.html',
	styleUrls: ['./faas-function-list-item.component.scss']
})
export class FaasFunctionListItemComponent implements OnInit {

	@Input()
	project: Project;

	@Input()
	selectedId: number;

	@Output()
	removeProject: EventEmitter<number> = new EventEmitter();

	@Output()
	changeProjectStatus: EventEmitter<number> = new EventEmitter();

	@Output()
	projectChosen: EventEmitter<number> = new EventEmitter();

	get classes(): string {
		return this.selectedId == this.project.id ? "container highlight-color" : "container";
	}

	constructor() { }

	ngOnInit(): void {
	}

	get areCommandsVisible(): boolean {
		return this.project.deploymentStatus == "RUNNING" ||
					this.project.deploymentStatus == "STOPPED";
	}

	get changeStatusButtonDisplay() {
		return this.project.deploymentStatus == "RUNNING" ? "pause" : "play_arrow";
	}

	get statusColor() {
		return this.project.deploymentStatus == "RUNNING" ? "green" : "red";
	}

	onProjectChosen() {
		this.projectChosen.emit(this.project.id);
	}

	onChangeProjectState() {
		this.changeProjectStatus.emit(this.project.id);
	}

	onRemoveProject() {
		this.removeProject.emit(this.project.id);
	}

}