import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'faas-url-popup',
	templateUrl: './url-popup.component.html',
	styleUrls: ['./url-popup.component.scss']
})
export class UrlPopupComponent implements OnInit {

	constructor(public dialogRef: MatDialogRef<UrlPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

	ngOnInit(): void {

	}

	close(): void {
		this.dialogRef.close();
	}

}
