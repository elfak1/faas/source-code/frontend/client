import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Project } from '../../models/project.model';

@Component({
	selector: 'faas-function-list',
	templateUrl: './faas-function-list.component.html',
	styleUrls: ['./faas-function-list.component.scss']
})
export class FaasFunctionListComponent implements OnInit {

	@Input()
	projects: Array<Project>;

	@Input()
	selectedId: number;

	@Output()
	removeProject: EventEmitter<number> = new EventEmitter();

	@Output()
	changeProjectStatus: EventEmitter<number> = new EventEmitter();

	@Output()
	projectChosen: EventEmitter<number> = new EventEmitter();

	constructor() {
	}

	ngOnInit(): void {
	}

	onChangeProjectState(event: number) {
		this.changeProjectStatus.emit(event);
	}

	onRemoveProject(event: number) {
		this.removeProject.emit(event);
	}

	onProjectChosen(event: number) {
		this.projectChosen.emit(event);
	}

}