import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/utility/services/auth.service';

@Component({
	selector: 'faas-account-settings',
	templateUrl: './account-settings.component.html',
	styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {

	userFormGroup: FormGroup;

	constructor(private loginService: AuthService, private fb: FormBuilder, public dialogRef: MatDialogRef<AccountSettingsComponent>, @Inject(MAT_DIALOG_DATA) public data: {}) {
		this.userFormGroup = this.createUserFormGroup();
	}

	ngOnInit(): void {
	}

	onClose() {
		this.dialogRef.close();
	}

	private createUserFormGroup() {
		console.log(this.loginService.user)
		const formGroup = this.fb.group({
			username: new FormControl(),
			email: new FormControl(this.loginService.user?.email),
			password: new FormControl(),
		});
		formGroup.disable();
		return formGroup;
	}

}
