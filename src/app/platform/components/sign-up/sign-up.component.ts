import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

import { User } from "../../models/user.model";
import { AuthService } from "src/app/utility/services/auth.service";

@Component({
	selector: 'faas-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
	userForm: FormGroup;

	constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

	ngOnInit(): void {
		this.userForm = this.createForm();
	}

	createForm(): FormGroup {
		return this.fb.group({
			username: new FormControl(),
			email: new FormControl("", [Validators.email]),
			password: new FormControl(),
			confirmpassword: new FormControl()
		});
	}

	public onCreateAccountClicked() {
		const user = new User();
		user.username = this.userForm.get("username").value;
		user.plainTextPassword = this.userForm.get("password").value;
		user.email = this.userForm.get("email").value;
		console.log(user);
		this.auth.signup(user).subscribe(x => {
			if (x)
				this.router.navigate(["../../platform/editor"]);
		}, error => alert("Signup attempt failed"));
	}
}