import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Dependency } from '../../models/dependency.model';

@Component({
	selector: 'faas-dependencies',
	templateUrl: './dependencies.component.html',
	styleUrls: ['./dependencies.component.scss']
})
export class DependenciesComponent {

	readonly: boolean;

	dependencyFormGroup: FormGroup;
	selectedDependency: Dependency;

	private _dependencyModels: Array<Dependency>;
	get dependencyModels(): Array<Dependency> {
		if (this._dependencyModels == null)
			this._dependencyModels = new Array<Dependency>();
		return this._dependencyModels;
	}
	set dependencyModels(value: Array<Dependency>) {
		this._dependencyModels = value;
		if (this._dependencyModels != null) {
			if (this.readonly)
				this.onSelectDependency(this.dependencyModels[0].name);
			this.refreshTableDisplay();
		}
	}

	private refreshTableDisplay() {
		this.dependenciesTableSource = this._dependencyModels.map((x, i) => { return { index: i, name: x.name } });
	}

	displayedColumns: string[] = ['index', 'name', 'actions'];
	dependenciesTableSource: Array<{ index: number, name: string }> = new Array();

	constructor(public dialogRef: MatDialogRef<DependenciesComponent>, @Inject(MAT_DIALOG_DATA) public data: { dependencies: Array<Dependency>, readonly: boolean }, private fb: FormBuilder) {
		this.dependencyFormGroup = this.createDependencyFormGroup();
		this.readonly = data.readonly;
		this.dependencyModels = data.dependencies;
	}

	createDependencyFormGroup(): FormGroup {
		return this.fb.group({
			name: new FormControl('', Validators.required),
			value: new FormControl('', Validators.required)
		});
	}

	get isConfirmDisabled(): boolean {
		if (!this.creatingNew || this.dependencyFormGroup.get('name').errors != null || this.dependencyFormGroup.get('value').errors != null)
			return true;
		return false;
	}

	onConfirm(): void {
		if (this.isConfirmDisabled)
			return;
		this.dependencyModels.push({
			name: this.dependencyFormGroup.get("name").value,
			value: this.dependencyFormGroup.get("value").value
		});
		this.refreshTableDisplay();
		this.dependencyFormGroup.reset();
	}

	creatingNew: boolean = true;
	get getName(): string {
		return this.creatingNew ? "New dependency" : this.dependencyFormGroup.get("name").value;
	}

	onSelectDependency(event: string) {
		this.creatingNew = false;
		const selected = this.dependencyModels.find(x => x.name == event);
		this.dependencyFormGroup.reset({
			name: selected.name,
			value: selected.value
		});
		this.dependencyFormGroup.disable();
	}

	onRemoveDependency(event: string) {
		const index = this.dependencyModels.findIndex(x => x.name == event);
		this.dependencyModels.splice(index, 1);
		this.refreshTableDisplay();
		if (this.dependencyFormGroup != null)
		{
			const name = this.dependencyFormGroup.get("name").value;
			if (name != null && name == event)
				this.onClear();
		}
	}

	onClear() {
		this.dependencyFormGroup.reset();
		this.dependencyFormGroup.enable();
		this.creatingNew = true;
	}

	onClose() {
		if (!this.readonly)
			this.dialogRef.close({data: this.dependencyModels});
		else
			this.dialogRef.close();
	}

}
