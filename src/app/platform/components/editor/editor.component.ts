import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { ProjectService } from 'src/app/utility/services/project.service';
import { CreateProjectRequest } from '../../models/create-project-request.model';
import { UrlPopupComponent } from '../url-popup/url-popup.component';

@Component({
	selector: 'faas-editor',
	templateUrl: './editor.component.html',
	styleUrls: ['./editor.component.scss']
})
export class EditorComponent {

	isBusy: boolean = false;

	constructor(private projectService: ProjectService, private dialog: MatDialog) { }

	onProjectSubmited(event: { request: CreateProjectRequest }) {
		this.isBusy = true;
		this.projectService.createProject(event.request).pipe(finalize(() => this.isBusy = false)).subscribe(x => {
			if (!x.error)
				this.dialog.open(UrlPopupComponent, { data: x.data });
		});
	}
}
