import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Router } from '@angular/router';
import { AuthService } from 'src/app/utility/services/auth.service';
import { AccountSettingsComponent } from '../account-settings/account-settings.component';

@Component({
	selector: 'faas-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	constructor(private router: Router, public dialog: MatDialog, private auth: AuthService) { }

	ngOnInit(): void {
	}

	onLogoutClicked() {
		this.auth.logOut();
		this.router.navigate(["/login"]);
	}

	onAccountSettings() {
		this.dialog.open(AccountSettingsComponent, { disableClose: true });
	}
}
