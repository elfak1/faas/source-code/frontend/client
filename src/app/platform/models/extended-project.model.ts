import { Dependency } from "./dependency.model";
import { Project } from "./project.model";

export class ExtendedProject extends Project {
	code: string;
	dependencies: Array<Dependency>;
	executeEndpoint: string;
	codeInfoEndpoint: string;
}