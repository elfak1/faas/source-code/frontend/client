export class Project {
	id: number;
	name: string;
	description: string;
	projectOwnerProfileId: number;
	programmingLanguage: string;
	deploymentStatus: string;
}