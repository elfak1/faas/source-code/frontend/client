import { Dependency } from "./dependency.model";
import { ProgrammingLanguage } from "./programming-language.enum";

export class CreateProjectRequest {
	language: ProgrammingLanguage;
	projectName: string;
	userCode: string;
	dependencies: Array<Dependency>;
}