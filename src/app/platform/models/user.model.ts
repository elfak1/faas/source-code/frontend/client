export class User {
	username: string;
	email: string;
	plainTextPassword: string;
	token: string;
}