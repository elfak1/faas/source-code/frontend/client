export const allFunctions = [
	{ description: "Array sum", status: "Running", code: `public static void Main()
	{
		int[] array = new int[]{ 1,2,3,4,5,6,7,8,9,10 }; 
		int sum = 0;
		for (int i = 0; i < array.length; i++)
			sum += array[i];
			
		System.out.print(sum);
	}` },
	{ description: "Array average", status: "Stopped", code: "Function 2 code" },
	{ description: "Matrix sum", status: "Running", code: "Function 3 code" },
	{ description: "Mean calculation", status: "Running", code: "Function 4 code" },
	// { description: "Method 1", status: "Running", code: "Method code 1" },
	// { description: "Method 2", status: "Stopped", code: "Method code 2" },
	// { description: "Method 3", status: "Running", code: "Method code 3" },
	// { description: "Method 4", status: "Running", code: "Method code 4" },
	// { description: "Aggregation fn 1", status: "Running", code: "Aggregation fn code 1" },
	// { description: "Aggregation fn 2", status: "Stopped", code: "Aggregation fn code 2" },
	// { description: "Aggregation fn 3", status: "Running", code: "Aggregation fn code 3" },
	// { description: "Aggregation fn 4", status: "Running", code: "Aggregation fn code 4" },
	// { description: "Custom fn 1", status: "Running", code: "Custom fn code 1" },
	// { description: "Custom fn 2", status: "Stopped", code: "Custom fn code 2" },
	// { description: "Custom fn 3", status: "Running", code: "Custom fn code 3" },
	// { description: "Custom fn 4", status: "Running", code: "Custom fn code 4" },
	// { description: "My function 1", status: "Running", code: "My function code 1" },
	// { description: "My function 2", status: "Stopped", code: "My function code 4" },
	// { description: "My function 3", status: "Running", code: "My function code 4" },
	// { description: "My function 4", status: "Running", code: "My function code 4" },
	// { description: "Corporate tax 1", status: "Running", code: "Corporate tax code 1" },
	// { description: "Corporate tax 2", status: "Stopped", code: "Corporate tax code 2" },
	// { description: "Corporate tax 3", status: "Running", code: "Corporate tax code 3" },
	// { description: "Corporate tax 4", status: "Running", code: "Corporate tax code 4" },
];

export const deps = [
	{name: "Sum aggregation dependency", value: `<dependency>
    <groupId>edu.xxx.cs</groupId>
    <artifactId>aggr</artifactId>
    <version>0.99</version>
    <scope>system</scope>
    <systemPath>basedir/aggr.jar</systemPath>
</dependency>`},
	{name: "Array dependency", value: `<dependency>
    <groupId>edu.xxx.cs</groupId>
    <artifactId>aggr</artifactId>
    <version>0.99</version>
    <scope>system</scope>
    <systemPath>basedir/aggr.jar</systemPath>
</dependency>`},
	{name: "Base java dependency", value: `<dependency>
    <groupId>edu.xxx.cs</groupId>
    <artifactId>aggr</artifactId>
    <version>0.99</version>
    <scope>system</scope>
    <systemPath>basedir/aggr.jar</systemPath>
</dependency>`},
	// {name: "Dependency 4", value: "Dependency 4 value"},
	// {name: "Dependency 5", value: "Dependency 5 value"},
	// {name: "Dependency 6", value: "Dependency 6 value"},
	// {name: "Dependency 7", value: "Dependency 7 value"},
	// {name: "Dependency 8", value: "Dependency 8 value"},
	// {name: "Dependency 9", value: "Dependency 9 value"}
];