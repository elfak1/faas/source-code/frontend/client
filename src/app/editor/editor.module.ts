import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MonacoEditorModule } from 'ngx-monaco-editor';

import { UtilityModule } from '../utility/utility.module';
import { EditorWrapperComponent } from './components/editor-wrapper/editor-wrapper.component';
import { DiffEditorWrapperComponent } from './components/diff-editor-wrapper/diff-editor-wrapper.component';
import { MaterialModule } from '../material.module';

@NgModule({
	declarations: [
		EditorWrapperComponent,
		DiffEditorWrapperComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MonacoEditorModule.forRoot(),
		UtilityModule,
		MaterialModule,
	],
	exports: [
		EditorWrapperComponent
	],
	providers: [
		EditorWrapperComponent
	]
})
export class EditorModule { }
