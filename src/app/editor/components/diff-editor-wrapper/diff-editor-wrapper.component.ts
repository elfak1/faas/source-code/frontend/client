import { Component } from '@angular/core';

import { DiffEditorModel } from 'ngx-monaco-editor';

@Component({
	selector: 'faas-diff-editor-wrapper',
	templateUrl: './diff-editor-wrapper.component.html',
	styleUrls: ['./diff-editor-wrapper.component.scss']
})
export class DiffEditorWrapperComponent {

	options = {
		theme: 'vs-dark'
	};
	originalModel: DiffEditorModel = {
		code: 'function asd() { return "asd" }',
		language: 'javascript'
	};

	modifiedModel: DiffEditorModel = {
		code: 'function hello() { return "hello" }',
		language: 'javascript'
	};
}
