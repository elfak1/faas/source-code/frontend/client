import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, Input, ViewChild, forwardRef, AfterViewInit } from '@angular/core';

import { EditorComponent } from 'ngx-monaco-editor';

@Component({
	selector: 'faas-editor-wrapper',
	templateUrl: './editor-wrapper.component.html',
	styleUrls: ['./editor-wrapper.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => EditorWrapperComponent),
			multi: true
		}
	]
})
export class EditorWrapperComponent implements ControlValueAccessor, AfterViewInit {

	@ViewChild(EditorComponent) editorComponent: EditorComponent;

	private _language: string;
	@Input() set language(value: string) {
		if (this._language !== value) {
			this._language = value;
			this.editorOptions = { ...this.editorOptions, language: this._language, automaticLayout: true };
		}
	}
	get language(): string {
		return this._language;
	}

	private _theme: string;
	@Input() set theme(value: string) {
		if (this._theme !== value) {
			this._theme = value;
			this.editorOptions = { ...this.editorOptions, theme: this._theme, automaticLayout: true };
		}
	}
	get theme(): string {
		return this._theme;
	}

	private _readOnly: boolean;
	@Input() set readOnly(value: boolean) {
		if (this._readOnly !== value) {
			this._readOnly = value;
			this.editorOptions = { ...this.editorOptions, readOnly: this._readOnly };
		}
	}
	get readOnly(): boolean {
		return this._readOnly;
	}

	editorOptions: any;
	value: string;
	disabled: boolean;

	constructor() { }

	ngAfterViewInit(): void {
		this.editorComponent.registerOnChange(this.onChange);
		this.editorComponent.registerOnTouched(this.onTouched);
	}

	//#region ControlValueAccessor
	writeValue(obj: any): void {
		this.value = obj;
	}
	onChange: () => {};
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	onTouched: () => {};
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}
	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}
	//#endregion
}
