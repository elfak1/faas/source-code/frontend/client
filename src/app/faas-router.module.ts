import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditorComponent } from './platform/components/editor/editor.component';
import { PlatformComponent } from './platform/platform.component';
import { LandingPageComponent } from './platform/components/landing-page/landing-page.component';
import { SignUpComponent } from './platform/components/sign-up/sign-up.component';
import { LogInComponent } from './platform/components/log-in/log-in.component';
import { FunctionsOverviewComponent } from './platform/components/functions-overview/functions-overview.component';
import { UserGuard } from './user.guard';

const routes: Routes = [
	{
		path: 'index',
		component: LandingPageComponent,
		children: [
			{ path: 'login', component: LogInComponent },
			{ path: 'signup', component: SignUpComponent }
		]
	},
	{
		path: 'platform',
		// canActivate: [UserGuard],
		// canActivateChild: [UserGuard],
		component: PlatformComponent,
		children: [
			{ path: 'editor', component: EditorComponent },
			{ path: 'functions', component: FunctionsOverviewComponent }
		]
	},
	{
		path: '**',
		redirectTo: 'index/login'
	}];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class FaasRouterModule {

}
